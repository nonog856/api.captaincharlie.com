import {
  IsEmail,
  IsOptional,
  IsString,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger'

export class EditUserDto {

  @ApiProperty({
    description: 'valid email for  user',
  })
  @IsEmail()
  @IsOptional()
  email?: string;

  @ApiProperty({
    description: 'valid firstName for  user',
  })
  @IsString()
  @IsOptional()
  firstName?: string;

  @ApiProperty({
    description: 'valid lastName for  user',
  })
  @IsString()
  @IsOptional()
  lastName?: string;
}
