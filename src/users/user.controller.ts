import { Controller, Get, Body, Patch, UseGuards, } from '@nestjs/common';
import { UserService } from './user.service';
import { JwtGuard } from '../auth/guard'
import { GetUser } from '../auth/decorator';
import { EditUserDto } from './dto';
import { User } from '@prisma/client';
import { ApiTags } from '@nestjs/swagger'

@ApiTags('User')
@UseGuards(JwtGuard)
@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('me')
  getMe(@GetUser() user: User) {
    return user;
  }

  @Patch()
  editUser(
      @GetUser('id') userId: string,
      @Body() dto: EditUserDto,
  ) {
    return this.userService.editUser(userId, dto);
  }

}
