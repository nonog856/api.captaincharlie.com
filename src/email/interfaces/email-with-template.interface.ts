import {MailDataRequired} from "@sendgrid/mail";
import { TemplateNamesE } from '../enums/email_templates.enum'


export interface EmailWithTemplateOptionsI {
    emailOptions: MailDataRequired,
    templateId: TemplateNamesE,
    context: any
}
