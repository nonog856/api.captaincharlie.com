import { MailDataRequired } from "@sendgrid/mail";


export interface EmailOptionsI {
    emailOptions: MailDataRequired
}
