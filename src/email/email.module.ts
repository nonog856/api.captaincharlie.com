import { Module } from '@nestjs/common';
import { EmailService } from './email.service';
import { EmailProcessor } from './jobs/email.processor'


@Module({
  imports: [],
  providers: [EmailService, EmailProcessor],
  exports: [EmailService]
})


export class EmailModule {}
