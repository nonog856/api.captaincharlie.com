import * as path from 'path'
import { Liquid } from 'liquidjs'
import { Process, Processor } from '@nestjs/bull'
import { EMAIL_QUEUE, SEND_EMAIL_FORGOT_PASSWORD, SEND_EMAIL_PASSWORD_CHANGED, SEND_EMAIL_SIGNUP_ACCOUNT_VERIFY } from '../../constants'
import { Job } from 'bull'
import { Logger } from '@nestjs/common'
import { EmailService } from '../email.service'
import { User } from '@prisma/client'
import { EmailOptionsI } from '../interfaces'
import { TemplateNamesE } from '../enums/email_templates.enum'


@Processor(EMAIL_QUEUE)
export class EmailProcessor {
    constructor(private emailService: EmailService) {}
    private readonly engine = new Liquid({ root: path.resolve(__dirname, '../templates/'), extname: '.liquid' })
    private readonly logger = new Logger(EmailProcessor.name, { timestamp: true });

    @Process(SEND_EMAIL_SIGNUP_ACCOUNT_VERIFY)
    async send_email_signup_account_verify (job: Job<User>) {
        try {
            const user = job.data
            const options = { emailOptions: {} } as EmailOptionsI
            const verify_email_url = `http://localhost:3000/auth/verify/email/${user.email_token}`

            options.emailOptions.to = user.email
            options.emailOptions.from = 'thecaptain@captaincharlie.mx'
            options.emailOptions.subject = 'Confirma tu cuenta de correo'
            options.emailOptions.text = 'Bienvenido a Captain Charly'

            options.emailOptions.html = await this.engine.renderFile(`${TemplateNamesE.CONFIRM_ACCOUNT_EAMIL_TEMPLATE + ".liquid"}`, { user, verify_email_url })
            await this.emailService.send_email_with_options(options)
        } catch (error) {
            this.logger.error(error)
        }
    }

    // CHAGE TEMPLATE TO FORGOT PASSWORD
    @Process(SEND_EMAIL_FORGOT_PASSWORD)
    async email_send_forgot_password (job: Job<User>) {
        try {
            const user = job.data
            const options = { emailOptions: {} } as EmailOptionsI
            const verify_email_url = `http://localhost:3000/auth/verify/email/${user.email_token}`

            options.emailOptions.to = user.email
            options.emailOptions.from = 'thecaptain@captaincharlie.mx'
            options.emailOptions.subject = 'Cambiar la conrasena'
            options.emailOptions.text = 'tu cuenta ha solicitado cambiar la contrasena'

            options.emailOptions.html = await this.engine.renderFile(`${TemplateNamesE.FORGOT_PASSWORD + ".liquid"}`, { user, verify_email_url })
            await this.emailService.send_email_with_options(options)
        } catch (error) {
            this.logger.error(error)
        }
    }

    // TEMPLATE TO PASWORD CHANGED TELPATE
    @Process(SEND_EMAIL_PASSWORD_CHANGED)
    async send_email_password_change (job: Job<User>) {
        try {
            const user = job.data
            const options = { emailOptions: {} } as EmailOptionsI

            options.emailOptions.to = user.email
            options.emailOptions.from = 'thecaptain@captaincharlie.mx'
            options.emailOptions.subject = 'Cambio de contrasena'
            options.emailOptions.text = 'Tu contrasena fue cambiada exitosamente'

            options.emailOptions.html = await this.engine.renderFile(`${TemplateNamesE.PASSWORD_CHANGED + ".liquid"}`, { user })
            await this.emailService.send_email_with_options(options)
        } catch (error) {
            this.logger.error(error)
        }
    }
}