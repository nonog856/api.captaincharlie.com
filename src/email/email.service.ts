import { Injectable, Logger } from '@nestjs/common'
import * as sgMail from '@sendgrid/mail'
import { EmailOptionsI } from './interfaces'
import { log } from 'util'

@Injectable()
export class EmailService {
    constructor() {}
    private readonly logger = new Logger(EmailService.name, { timestamp: true })
    private SENDGRID_API_KEY = process.env.SENDGRID_API_KEY


    public async send_email_with_options(emailConfiguration: EmailOptionsI): Promise<void> {
        try {
            const { emailOptions } = emailConfiguration

            sgMail.setApiKey(this.SENDGRID_API_KEY)
            await sgMail.send(emailOptions)
        }
        catch (e) {
            this.logger.error(e)
        }
        return
    }
}