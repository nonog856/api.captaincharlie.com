import { Injectable, UseGuards } from '@nestjs/common'
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { PrismaService } from '../prisma/prisma.service'
import { Product } from "prisma"
import { Logger } from '@nestjs/common'

@Injectable()
export class ProductService {
  constructor(private prisma: PrismaService) {}
  private readonly logger = new Logger(ProductService.name, {timestamp: true});


  public async create(createProductDto: CreateProductDto) {
    return 'This action adds a new product';
  }

  public async findAll() {
    return this.prisma.product.findMany(
        {
          where: {
            is_in_stock: true,
            is_active: true
          }
        }
    );
  }

  public async findOne(id: string): Promise<Product> {
    return this.prisma.product.findUnique({
      where: {
        id: id
      }
    })
  }

  public async update(id: string, updateProductDto: UpdateProductDto) {
    return `This action updates a #${id} product`;
  }

  public async remove(id: string) {
    return `This action removes a #${id} product`;
  }
}
