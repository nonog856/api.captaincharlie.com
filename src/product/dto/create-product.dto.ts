
import { IsBoolean, IsEmail, IsInt, IsNotEmpty, IsString, MinLength } from 'class-validator'
import { ApiProperty } from '@nestjs/swagger'


export class CreateProductDto {
    @ApiProperty({ description: 'name of the product', })
    @IsString()
    @IsNotEmpty()
    name: string;

    @ApiProperty({ description: 'mesure of the product', })
    @IsString()
    @IsNotEmpty()
    @MinLength(8)
    measure: string;

    @ApiProperty({ description: 'price of the product', })
    @IsString()
    @IsNotEmpty()
    price: string;

    @ApiProperty({ description: 'image_url of the product', })
    @IsString()
    image_url: string;

    @ApiProperty({ description: 'weight of the product', })
    @IsString()
    weight: string;

    @ApiProperty({ description: 'description of the product', })
    @IsString()
    @IsNotEmpty()
    description: string;

    @ApiProperty({ description: 'is_in_stock of the product', })
    @IsBoolean()
    is_in_stock: boolean;

    @ApiProperty({ description: 'is_active of the product', })
    @IsBoolean()
    is_active: boolean;

    @ApiProperty({ description: 'minimum of the product', })
    @IsInt()
    minimum: number;

    @ApiProperty({ description: 'unit of the product', })
    @IsInt()
    unit: number;
}
