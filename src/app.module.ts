import { Global, Module } from '@nestjs/common'
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { BullModule } from '@nestjs/bull';
import { UserModule } from './users/user.module';
import { AuthModule } from './auth/auth.module';
import { PrismaModule } from './prisma/prisma.module';
import { PrismaService } from './prisma/prisma.service'
import { EmailService } from './email/email.service';
import { EmailModule } from './email/email.module';
import { EMAIL_QUEUE } from './constants'
import { ProductModule } from './product/product.module';
import { StoreModule } from './store/store.module';


@Global()
@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true, }),
    BullModule.forRoot({
      redis: { host: 'localhost', port: parseInt(process.env.REDIS_PORT) },
    }),
    BullModule.registerQueueAsync({ name: EMAIL_QUEUE }),
    UserModule,
    AuthModule,
    PrismaModule,
    EmailModule,
    ProductModule,
    StoreModule,
  ],
  exports:[BullModule],
  controllers: [AppController],
  providers: [AppService, PrismaService, EmailService],
})
export class AppModule {}
