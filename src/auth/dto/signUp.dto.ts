import {
  IsEmail,
  IsLowercase,
  IsMobilePhone,
  IsNotEmpty,
  IsString,
  MinLength,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger'

export class SignUpDto {

  @ApiProperty({
    description: 'valid email for user',
  })
  @IsEmail()
  @IsString()
  @IsNotEmpty()
  email: string;

  @ApiProperty({
    description: 'valid password for  user',
  })
  @IsString()
  @IsNotEmpty()
  @MinLength(8)
  password: string;

  @ApiProperty({
    description: 'valid username for  user',
  })
  @IsString()
  @IsNotEmpty()
  @IsLowercase()
  @MinLength(4)
  username: string;

  @ApiProperty({
    description: 'valid first_name for  user',
  })
  @IsString()
  @IsNotEmpty()
  @MinLength(2)
  first_name: string;

  @ApiProperty({
    description: 'valid last_name for  user',
  })
  @IsString()
  @IsNotEmpty()
  @MinLength(2)
  last_name: string;

  @ApiProperty({
    description: 'valid phone for  user',
  })
  @IsMobilePhone()
  phone: string;
}
