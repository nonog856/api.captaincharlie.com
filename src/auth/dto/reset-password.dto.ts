import { IsEmail, IsJWT, IsNotEmpty, IsString } from 'class-validator'
import { ApiProperty } from '@nestjs/swagger'

export class ResetPasswordDto {

    @IsString()
    @IsNotEmpty()
    @ApiProperty({ description: 'password of the valid  user', })
    password: string;

    @IsJWT()
    @IsNotEmpty()
    @ApiProperty({ description: 'jwt for validation', })
    token: string;
}
