import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger'

export class SignInDto {
  @ApiProperty({
      description: 'email of the valid  user',
  })
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty({
    description: 'password of the valid  user',
  })
  @IsString()
  @IsNotEmpty()
  @MinLength(8)
  password: string;
}
