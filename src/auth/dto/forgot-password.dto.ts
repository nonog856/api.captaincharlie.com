import { IsEmail, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger'

export class ForgotPasswordDto {
    @ApiProperty({ description: 'email of the valid  user', })
    @IsEmail()
    @IsNotEmpty()
    email: string;
}
