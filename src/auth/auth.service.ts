import * as argon from 'argon2'

import { BadRequestException, Body, ForbiddenException, Injectable, Logger } from '@nestjs/common'
import { PrismaService } from '../prisma/prisma.service'
import { ResetPasswordDto, ForgotPasswordDto, SignInDto, SignUpDto } from './dto'
import { PrismaClientKnownRequestError } from '@prisma/client/runtime'
import { JwtService } from '@nestjs/jwt'
import { SignInResponse } from './interface/signInResponse'
import { InjectQueue } from '@nestjs/bull'
import { Queue } from 'bull'
import { EMAIL_QUEUE, SEND_EMAIL_FORGOT_PASSWORD, SEND_EMAIL_PASSWORD_CHANGED, SEND_EMAIL_SIGNUP_ACCOUNT_VERIFY } from '../constants'
import { User } from '@prisma/client'

@Injectable()
export class AuthService {
    constructor(private prisma: PrismaService, private jwt: JwtService, @InjectQueue(EMAIL_QUEUE) private emailQueue: Queue) {}

    private readonly jwt_seassion_secret = process.env.JWT_SEASSION_SECRET
    private readonly jwt_password_reset_secret = process.env.JWT_PASSWORD_RESET_SECRET
    private readonly jwt_account_verification_secret = process.env.JWT_ACCOUNT_VERIFICATION_SECRET

    private readonly logger = new Logger(AuthService.name, { timestamp: true })

    private async signToken(sub: string, expiresIn: string = '30m'): Promise<string> {
        const payload = { sub: sub }
        return this.jwt.signAsync(payload, { expiresIn: expiresIn, secret: this.jwt_seassion_secret })
    }

    public async signUp(@Body() dto: SignUpDto): Promise<void> {
        try {
            const password = await argon.hash(dto.password)
            const email_token = await this.signToken(dto.email, '60min')

            const user = await this.prisma.user.create({ data: {
                    username: dto.username,
                    email: dto.email,
                    first_name: dto.first_name,
                    last_name: dto.last_name,
                    phone: dto.phone,
                    password,
                    email_token,
                }
            })

            await this.emailQueue.add(SEND_EMAIL_SIGNUP_ACCOUNT_VERIFY, { ...user }, { delay: 10000 })
        }
        catch (error) {
            if (error instanceof PrismaClientKnownRequestError) {
                if (error.code === 'P2002') {
                    throw new ForbiddenException('Credentials taken')
                }
            }
            throw error
        }
    }

    public async signIn(@Body() dto: SignInDto): Promise<SignInResponse> {
        const user = await this.prisma.user.findUnique({ where: { email: dto.email } })
        if (!user) throw new ForbiddenException({ message: 'Credenciales no validas' })

        const pwMatches = await argon.verify(user.password, dto.password)
        if (!pwMatches) throw new ForbiddenException({ message: 'Credenciales no validas' })

        const token = await this.signToken(user.id)
        return { access_token: token }
    }

    public async verify_eamil(token: string) {
        try {
            const verify = await this.jwt.verify(token, { secret: this.jwt_seassion_secret })

            const email = verify.sub
            const user = await this.prisma.user.findUnique({ where: { email } })

            if (user.is_email_verified) throw new BadRequestException({ message: 'Tu cuenta ya fue activada.' })
            if (user.email_token != token) throw new BadRequestException({ message: 'este url de activacion on es valido intenta otro' })

            await this.prisma.user.update({ where: { email }, data: { is_email_verified: true, email_token: null } })
            this.logger.debug(`Account verified for ${user.email}`)

            return { status: 200}
        } catch (error) {
            throw new BadRequestException({ message: 'email de activacion expiro.' })
        }
    }

    public async resend_email_token(user: User) {
        try {
            if (user.is_email_verified) {
                if (user.email_token) await this.prisma.user.update({ where: { email: user.email }, data: { email_token: null } })
                throw new BadRequestException({ message: 'tu cuenta ya fue validada' })
            }

            const email_token = await this.signToken(user.email, '60min')
            await this.prisma.user.update({ where: { email: user.email }, data: { email_token } })
            await this.emailQueue.add(SEND_EMAIL_SIGNUP_ACCOUNT_VERIFY, { ...user }, { delay: 10000 })
            return { status: 200}
        } catch (error) {
            throw new BadRequestException({ message: 'hubo un error al validar al cucenta' })
        }
    }

    public async forgot_password(dto: ForgotPasswordDto) {
        const user = await this.prisma.user.findUnique({ where: { email: dto.email } })

        if (!user) throw new BadRequestException({ message: "Verifica qeu la informacion sea correcta" })

        const reset_password_token = await this.signToken(user.email, '60min')
        await this.prisma.user.update({ where: { email: user.email }, data: { reset_password_token } })

        await this.emailQueue.add(SEND_EMAIL_FORGOT_PASSWORD, { ...user }, { delay: 10000 })
        return { status: 200}
    }

    public async reset_password(dto: ResetPasswordDto) {
        try {
            const verify = await this.jwt.verify(dto.token, { secret: this.jwt_seassion_secret })

            const email = verify.sub
            const user = await this.prisma.user.findUnique({ where: { email } })

            if (!user) throw new BadRequestException({ message: "try resetting your password again" })
            if (user.reset_password_token != dto.token) throw new BadRequestException({ message: "try resetting your password again" })

            const password = await argon.hash(dto.password)
            await this.prisma.user.update({ where: { email: user.email }, data: { password, reset_password_token: null } })

            await this.emailQueue.add(SEND_EMAIL_PASSWORD_CHANGED, { ...user }, { delay: 10000 })

            return { status: 200 }
        } catch (error) {
            throw new BadRequestException({message: "try resetign your password again"})
        }
    }
}


