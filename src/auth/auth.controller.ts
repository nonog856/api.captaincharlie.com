import { Body, Controller, Get, HttpCode, HttpStatus, Param, Post, UseGuards } from '@nestjs/common'
import { AuthService } from './auth.service';
import { ResetPasswordDto, SignInDto, SignUpDto } from './dto'
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger'
import { SignInResponse } from './interface/signInResponse'
import { JwtGuard } from './guard'
import { GetUser } from './decorator'
import { User } from '@prisma/client'
import { ForgotPasswordDto } from './dto/forgot-password.dto'

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('signup')
  @HttpCode(204)
  @ApiOperation({ summary: 'Signup user' })
  @ApiResponse({ status: 201, description: 'Created.', })
  @ApiResponse({ status: 403, description: 'Forbidden.', })
  async signUp(@Body() dto: SignUpDto): Promise<void>  {
    return await this.authService.signUp(dto);
  }

  @Post('signin')
  @ApiOperation({ summary: 'Signin user' })
  @ApiResponse({ status: 401, description: 'Forbidden.' })
  @ApiResponse({ status: 200, description: 'Signed in Successful.', })
  async signIn(@Body() dto: SignInDto): Promise<SignInResponse> {
    return this.authService.signIn(dto);
  }

  @ApiResponse({ status: 200, description: 'Email Sent.', })
  @ApiResponse({ status: 400, description: 'Error of token.', })
  @ApiOperation({ summary: 'confirm account' })
  @Get('verify/email/:token')
  async verify_eamil (@Param('token') token: string) {
    return this.authService.verify_eamil(token)
  }

  @UseGuards(JwtGuard)
  @ApiResponse({ status: 200, description: 'Email Sent.', })
  @ApiResponse({ status: 400, description: 'Error of token.', })
  @ApiOperation({ summary: 'resend account verification' })
  @Get('resend/email/')
  async resend_email_token (@GetUser() user: User) {
    return this.authService.resend_email_token(user)
  }

  @ApiOperation({ summary: 'Forgot password' })
  @ApiResponse({ status: 200, description: 'Email Sent.', })
  @ApiResponse({ status: 400, description: 'Error of token.', })
  @Post('forgot-password')
  async forgot_password (@Body() dto: ForgotPasswordDto) {
    return this.authService.forgot_password(dto)
  }

  @ApiOperation({ summary: 'Reset Password' })
  @ApiResponse({ status: 200, description: 'Email Sent.', })
  @ApiResponse({ status: 400, description: 'Error of token.', })
  @Post('forgot-password/reset')
  async reset_password (@Body() dto: ResetPasswordDto) {
    return this.authService.reset_password(dto)
  }
}
